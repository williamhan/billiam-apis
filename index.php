<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Bulk SMS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
    </style>
    <link href="bootstrap/css/bootstrap-responsive.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  </head>

  <body>

    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="#">Unsub Test Page</a>
        </div>
      </div>
    </div>

    <div class="container">

      <!-- Main hero unit for a primary marketing message or call to action -->
      <div class="hero-unit">
        <h1><b>Unsub Test Page</b></h1>
        <p>Still a work in progress.</p>
        <form enctype="multipart/form-data" class="well" action="classtest.php" method="POST">
		      <!-- <input type="hidden" name="MAX_FILE_SIZE" value="100000000" />
          <input name="csv_file" type="file" />
 -->
          <div class="form-group">
<!--
  <label for="comment">Enter Text Message Here:</label>
  <textarea class="form-control" rows="5" id="comment" name="textmessage"></textarea>
-->
</div>

<!-- <label class="radio-inline"><input type="radio" name="messageType" value="SMS">SMS</label> -->
<!-- <label class="radio-inline"><input type="radio" name="messageType" value="MMS">MMS</label> -->

<!--
<div class="form-group">
  <label for="img_file">Media File (Must Be Less Than 5MB):</label>
  <input name="img_file" type="file" />
</div>
-->
<!-- <div class="form-group">
  <label for="comment">Set Batch Size Here:</label>
  <textarea class="form-control" rows="1" id="batchsize" name="batchsize"></textarea>
</div> -->

<div class="form-group">
  <label for="comment">Enter Start date. (yyyy-mm-dd format)</label>
  <input type="text" id="startdate" name="startdate" class="form-control">

  <label for="comment">Enter End date. (yyyy-mm-dd format)</label>
  <input type="text" id="enddate" name="enddate" class="form-control">

</div>

<!--
<div class="form-group">
  <label for="comment">Set Time Interval (minutes) Here:</label>
  <textarea class="form-control" rows="1" id="interval" name="interval"></textarea>
</div>
-->

<input type="submit" name="submit" value="Run Unsub Scrub"/>

	</form>
	<!-- <p><a href="http://www.twilio.com/" style="text: decoration: none; display: inline-block; width: 166px; height: 0; overflow: hidden; padding-top: 31px; background: url(http://www.twilio.com/packages/company/img/logos_icon_poweredbysmall.png) no-repeat;">powered by twilio™</a></p> -->
      </div>

      <hr>

      <footer>
        <p>&copy; Billiam2016</p>
      </footer>

    </div> <!-- /container -->


    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="bootstrap/js/bootstrap-modal.js"></script>


  </body>
</html>