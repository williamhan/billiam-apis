<?php
require __DIR__ . '/vendor/autoload.php';
use Twilio\Rest\Client;

function in_arrayi($needle, $haystack) {
        return in_array(strtolower($needle), array_map('strtolower', $haystack));
    }

class Conntomysql
{
	public $connection;
	protected $servername = "107.180.51.77";
	protected $username = "drd_1";
	protected $password = "Us3th3forc3!";
	protected $dbname = "twilionono";
	protected $from_number;
	protected $to_number;
	protected $msid;
	protected $body;


	public function __construct()
	{
		$this->connection = new mysqli($this->servername,$this->username,$this->password,$this->dbname);
		// Error handling
		if (mysqli_connect_errno()) 
		{ 
			die("Database connection failed: " . 
			mysqli_connect_error() . " (" . 
			mysqli_connect_errno() . ")" ); 
		} 
	}

	// Magic method clone is empty to prevent duplication of connection
	private function __clone() {}

	// Get mysqli connection
	public function getConnection() 
	{
		return $this->connection;
	}

	public function insertunsub($from_number,$to_number,$msid,$body,$msid_name)
	{
		$res = mysqli_query($this->connection, "INSERT opt_out_numbers(from_number,to_number,body,msid,msid_name) VALUES('$from_number','$to_number','$body','$msid','$msid_name')");
		return $res;
	}

	public function closeconn() 
	{
    	$this->connection->close();
	}
}

class TwilConn
{
	public $client;
	protected $account_sid = "AC3b43cbf2ce888f1a132d5ef679af8975";
	protected $auth_token = "9849468234fb3118ec2f8c46cdecf3dd";
	public $STOP_KEYWORDS = ['STOP', 'STOPALL', 'UNSUBSCRIBE', 'CANCEL', 'END', 'QUIT'];

	public function __construct()
	{
		$this->client = new Client($this->account_sid, $this->auth_token);
	}


	public function getmsgsrvcname($msgserviceid)
	{
		$srvc = $this->client
			->messaging
			->v1
			->services($msgserviceid)
			->fetch();

		return $srvc->friendlyName;

	}




	public function getibsms($datesentstart, $datesentend)
	{
		
		//$params = array('direction' => 'inbound', 'dateSentAfter' => '2017-08-01');
		$smsmessages = array();
		$smsmessages = $this->client
			->messages
			//->read($params);
    		 ->read(
         		array(
         			"direction" => "inbound",
         			"dateSentAfter" => date($datesentstart),
         			"dateSentBefore" => date($datesentend)
    		 	)
    		);



        return $smsmessages;
	}

	
}